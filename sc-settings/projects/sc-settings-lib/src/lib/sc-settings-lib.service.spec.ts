import { TestBed } from '@angular/core/testing';

import { ScSettingsLibService } from './sc-settings-lib.service';

describe('ScSettingsLibService', () => {
  let service: ScSettingsLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScSettingsLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
