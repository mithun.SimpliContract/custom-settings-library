import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'lib-sc-settings-lib',
  template: `
    <h2>
      This is settings library
    </h2>
    <button class="toggle" (click)="test()">Switch theme</button>
  `,
  styles: []
})
export class ScSettingsLibComponent implements OnInit {

  @Output( )
  themeSetter = new EventEmitter<boolean>();

  toggle: boolean = true;
  theme: string = 'dark-theme'

  constructor() { }

  ngOnInit(): void {
  }

  test() {
    this.toggle = !this.toggle
    this.themeSetter.emit(this.toggle)
  }

}
