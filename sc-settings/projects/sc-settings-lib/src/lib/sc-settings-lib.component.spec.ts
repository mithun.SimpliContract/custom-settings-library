import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScSettingsLibComponent } from './sc-settings-lib.component';

describe('ScSettingsLibComponent', () => {
  let component: ScSettingsLibComponent;
  let fixture: ComponentFixture<ScSettingsLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScSettingsLibComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScSettingsLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
