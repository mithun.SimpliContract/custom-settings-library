import { NgModule } from '@angular/core';
import { ScSettingsLibComponent } from './sc-settings-lib.component';



@NgModule({
  declarations: [
    ScSettingsLibComponent
  ],
  imports: [
  ],
  exports: [
    ScSettingsLibComponent
  ]
})
export class ScSettingsLibModule { }
