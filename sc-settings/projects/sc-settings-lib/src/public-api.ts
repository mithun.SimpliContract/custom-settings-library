/*
 * Public API Surface of sc-settings-lib
 */

export * from './lib/sc-settings-lib.service';
export * from './lib/sc-settings-lib.component';
export * from './lib/sc-settings-lib.module';
