import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import  {ScSettingsLibModule} from 'sc-settings-lib'

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ScSettingsLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
