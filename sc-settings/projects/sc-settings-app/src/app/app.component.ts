import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'sc-settings-app';

  theme: Theme = 'light-theme';

  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2) {}

  ngOnInit() {
    this.updateTheme()
  }

  updateTheme() {
    this.renderer.addClass(this.document.body,this.theme);
  }
  libEvent(event: any) {
    this.document.body.classList.replace(this.theme, event === true ? this.theme = 'dark-theme' : this.theme='light-theme')
    
  }
}

export type Theme = 'light-theme' | 'dark-theme';
